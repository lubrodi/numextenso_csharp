SET FRAMEWORK_VERSION=2.2
SET Languages=src\Lamkr.NumExtenso.Languages
SET Application=src\Lamkr.NumExtenso.Application
SET Domain=src\Lamkr.NumExtenso.Domain
SET Api=src\Lamkr.NumExtenso.Api

dotnet publish -c Release %Languages%
dotnet publish -c Release %Application%
dotnet publish -c Release %Domain%
dotnet publish -c Release %Api%

mkdir app

copy /y %Languages%\bin\Release\netcoreapp%FRAMEWORK_VERSION%\publish\*.dll app
copy /y %Application%\bin\Release\netcoreapp%FRAMEWORK_VERSION%\publish\*.dll app
copy /y %Domain%\bin\Release\netcoreapp%FRAMEWORK_VERSION%\publish\*.dll app
copy /y %Api%\bin\Release\netcoreapp%FRAMEWORK_VERSION%\publish\*.dll app
copy /y %Api%\bin\Release\netcoreapp%FRAMEWORK_VERSION%\publish\web.config app
copy /y %Api%\bin\Release\netcoreapp%FRAMEWORK_VERSION%\publish\*.json app

cd app
dotnet Lamkr.NumExtenso.Api.dll




