# Lamk.NumExtenso (.NET Core C#)
API para descrever um número por extenso.

## Passos iniciais
Essas instruções ajudarão você a copiar o projeto para execução em sua máquina de desenvolvimento.

### Pré-requisitos
* Instalar o [.NET Core 2.2 SDK](https://dotnet.microsoft.com/download/dotnet-core/2.2).
* Opcional: instalar o [Visual Studio Community 2019](https://www.visualstudio.com/).

### Compilação/execução (Visual Studio)
* Abra o arquivo ``Lamkr.NumExtenso.sln`` no Visual Studio IDE
* Escolha a opção de execução referente ao projeto, conforme a imagem abaixo:
<p align="center">
 <img width="460" height="300" src="opcao.png">
</p>

* Compile e execute o projeto diretamente na IDE.

### Compilação/execução (dotnet)
* Na linha do comando do sistema operacional, vá até à pasta do projeto ``Lamkr.NumExtenso.Api`` e execute o comando:
```dotnet run --urls=http://*:3000/```

### Exemplos
Uma vez que o projeto de API está em execução, abra um navegador, e execute essas requisições de exemplo:
  * http://localhost:3000/1
  * http://localhost:3000/-1042
  * http://localhost:3000/401
  * http://localhost:3000/99999
 
Os resultados retornados serão objetos Json que descrevem por extenso o número informado. O resultado dos testes acima serão, respectivamente:
```
{ "extenso": "um" } 
```
```
{ "extenso": "menos mil e quarenta e dois" } 
```
```
{ "extenso": "quatrocentos e um" } 
```
```
{"extenso":"noventa e nove mil e novecentos e noventa e nove"}
```




