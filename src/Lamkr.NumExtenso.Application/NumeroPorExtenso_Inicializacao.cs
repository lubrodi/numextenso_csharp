﻿using Lamkr.NumExtenso.Domain;
using System.Collections.Generic;
using System.Text;
using Lamkr.NumExtenso.Languages;

namespace Lamkr.NumExtenso.Application
{
    public partial class NumeroPorExtenso : INumeroPorExtensoService
    {
		public const string NumeroInvalido = "Número inválido";

        public const int PrimeiraClasse = 1, SegundaClasse = 2;

		public readonly Dictionary<long, string> Cardinais = new Dictionary<long, string>();

		public long InicioFaixa { get; }
        public long FimFaixa { get; }

		public NumeroPorExtenso(long inicioFaixa = -99999, long fimFaixa = 99999) {
			InicioFaixa = inicioFaixa;
			FimFaixa = fimFaixa;
			MontarCardinais();
		}

		public void MontarCardinaisPorRecurso() {
            string conteudo = Resources.Cardinais;
            byte[] bytes = Encoding.Default.GetBytes(conteudo);
            conteudo = Encoding.UTF8.GetString(bytes);
            string[] linhas = conteudo.Split("\r\n");
            foreach (string linha in linhas) {
                if (!string.IsNullOrEmpty(linha))
                {
                    string[] cardinal = linha.Split(',');
                    Cardinais.Add(long.Parse(cardinal[0]), cardinal[1]);
                }
			}
		}

        public void MontarCardinais() {
            Cardinais[1] = NumerosCardinais.Um;
            Cardinais[2] = NumerosCardinais.Dois;
            Cardinais[3] = NumerosCardinais.Tres;
            Cardinais[4] = NumerosCardinais.quatro;
            Cardinais[5] = NumerosCardinais.Cinco;
            Cardinais[6] = NumerosCardinais.Seis;
            Cardinais[7] = NumerosCardinais.Sete;
            Cardinais[8] = NumerosCardinais.Oito;
            Cardinais[9] = NumerosCardinais.Nove;
            Cardinais[10] = NumerosCardinais.Dez;
            Cardinais[11] = NumerosCardinais.Onze;
            Cardinais[12] = NumerosCardinais.Doze;
            Cardinais[13] = NumerosCardinais.Treze;
            Cardinais[14] = NumerosCardinais.Quatorze;
            Cardinais[15] = NumerosCardinais.Quinze;
            Cardinais[16] = NumerosCardinais.Dezesseis;
            Cardinais[17] = NumerosCardinais.Dezessete;
            Cardinais[18] = NumerosCardinais.Dezoito;
            Cardinais[19] = NumerosCardinais.Dezenove;
            Cardinais[20] = NumerosCardinais.Vinte;
            Cardinais[30] = NumerosCardinais.Trinta;
            Cardinais[40] = NumerosCardinais.Quarenta;
            Cardinais[50] = NumerosCardinais.Cinquenta;
            Cardinais[60] = NumerosCardinais.Sessenta;
            Cardinais[70] = NumerosCardinais.Setenta;
            Cardinais[80] = NumerosCardinais.Oitenta;
            Cardinais[90] = NumerosCardinais.Noventa;
            Cardinais[100] = NumerosCardinais.Cento;
            Cardinais[200] = NumerosCardinais.Duzentos;
            Cardinais[300] = NumerosCardinais.Trezentos;
            Cardinais[400] = NumerosCardinais.Quatrocentos;
            Cardinais[500] = NumerosCardinais.Quinhentos;
            Cardinais[600] = NumerosCardinais.Seiscentos;
            Cardinais[700] = NumerosCardinais.Setecentos;
            Cardinais[800] = NumerosCardinais.Oitocentos;
            Cardinais[900] = NumerosCardinais.Novecentos;
        }
    }
}
