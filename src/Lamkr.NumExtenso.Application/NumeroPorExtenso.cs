﻿using Lamkr.NumExtenso.Languages;
using System.Text;

namespace Lamkr.NumExtenso.Application
{
    public partial class NumeroPorExtenso
    {
        public string DescreverNumero(long numero)
        {
            if (numero < InicioFaixa || numero > FimFaixa)
                return NumeroInvalido;

			if (numero == 0)
				return Resources.Zero;

			string descricao = string.Empty;

            if (numero < 0) {
                descricao = Resources.Menos;
                numero = 0 - numero;
            }

			descricao += MontarDescricao(numero);

			return descricao.Trim().Replace("  ", " "); //!
        }

		public string MontarDescricao(long numero) {
            StringBuilder strBuilder = new StringBuilder();
            string descricao = string.Empty;
            int classe = PrimeiraClasse;
			while( numero != 0 )
			{
                numero = ExtrairUltimaCentena(numero, out int valor);
                descricao = DescreverCentena(valor); //! += ClasseCardinal(classe);
                //! Adicionado teste para ignorar o "um" no caso de estar isolado numa classe, como 1042, por exemplo.
                //! Na verdade, o correto seria escrever "um mil e quarenta e dois" e não "mil e quarenta e dois".
                if (classe > PrimeiraClasse && valor == 1)
                    descricao = "";
                descricao += ClasseCardinal(classe);
                //!
                if (strBuilder.Length > 0)
                    strBuilder.Insert(0, Resources.Conjuncao_E);
                strBuilder.Insert(0, descricao);
                classe++;
			}
            return strBuilder.ToString();
		}

        public string ClasseCardinal(long classe) {
            if (classe > PrimeiraClasse)
                return Resources.Mil;
            return string.Empty;
        }

        public long ExtrairUltimaCentena(long numero, out int centena) {
            centena = (int) (numero % 1000);
            return numero / 1000;
        }

        public string DescreverCentena(int valor) {
            string descricao = string.Empty;
            if (PossuiDescricaoCardinal(valor, ref descricao))
                return descricao;

            int centena = valor / 100 * 100;
            descricao += centena == 0 ? string.Empty : Cardinais[centena];

            int dezena = valor - centena;
            string descricaoDezena = string.Empty;
            if (PossuiDescricaoCardinal(dezena, ref descricaoDezena)) {
                return descricao + (descricao.Length > 0 ? Resources.Conjuncao_E : string.Empty) + descricaoDezena;
            }
            else if( dezena > 0 ) {
                dezena = dezena / 10 * 10;
                descricao += (descricao.Length > 0 ? Resources.Conjuncao_E : string.Empty) + Cardinais[dezena];
            }

            int unidade = valor % 10;
            descricao += (descricao.Length > 0 ? Resources.Conjuncao_E : string.Empty) + Cardinais[unidade];

            return descricao;
		}

        public bool PossuiDescricaoCardinal(int valor, ref string descricao) {
            if (valor == 0) { 
                descricao = string.Empty;
                return true;
            }
            else if (valor == 100) {
                descricao = Resources.Cem;
                return true;
            }
            if (!Cardinais.ContainsKey(valor))
                return false;
            descricao = (descricao.Length > 0 ? Resources.Conjuncao_E : string.Empty) + Cardinais[valor];
            return true;
        }
    }
}
