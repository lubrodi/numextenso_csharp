using Xunit;

namespace Lamkr.NumExtenso.Application.Tests.Unit
{
    public partial class NumeroPorExtensoTests
    {
        [Fact]
        public void NumeroPorExtenso_DescreverCentena_testar_centenas()
        {
			string extensao;

			extensao = _numeroPorExtenso.DescreverCentena(100);
			Assert.Equal("cem", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(101);
            Assert.Equal("cento e um", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(110);
            Assert.Equal("cento e dez", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(111);
            Assert.Equal("cento e onze", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(150);
            Assert.Equal("cento e cinquenta", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(153);
            Assert.Equal("cento e cinquenta e tr�s", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(199);
            Assert.Equal("cento e noventa e nove", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(200);
            Assert.Equal("duzentos", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(234);
            Assert.Equal("duzentos e trinta e quatro", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(999);
            Assert.Equal("novecentos e noventa e nove", extensao);
        }
    }
}
