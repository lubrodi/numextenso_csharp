using System.Collections.Generic;
using Xunit;

namespace Lamkr.NumExtenso.Application.Tests.Unit
{
    public partial class NumeroPorExtensoTests
    {
        [Fact]
        public void NumeroPorExtenso_DescreverCentena_testar_milhar()
        {
            Assert.Throws<KeyNotFoundException>(() => _numeroPorExtenso.DescreverCentena(1000));
        }
    }
}
