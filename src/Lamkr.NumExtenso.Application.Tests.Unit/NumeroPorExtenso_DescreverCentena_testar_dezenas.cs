using Xunit;

namespace Lamkr.NumExtenso.Application.Tests.Unit
{
    public partial class NumeroPorExtensoTests
    {
        [Fact]
        public void NumeroPorExtenso_DescreverCentena_testar_dezenas()
        {
			string extensao;

			extensao = _numeroPorExtenso.DescreverCentena(10);
			Assert.Equal("dez", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(11);
            Assert.Equal("onze", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(12);
            Assert.Equal("doze", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(13);
            Assert.Equal("treze", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(14);
            Assert.Equal("quatorze", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(15);
            Assert.Equal("quinze", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(16);
            Assert.Equal("dezesseis", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(17);
            Assert.Equal("dezessete", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(18);
            Assert.Equal("dezoito", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(19);
            Assert.Equal("dezenove", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(20);
            Assert.Equal("vinte", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(21);
            Assert.Equal("vinte e um", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(30);
            Assert.Equal("trinta", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(32);
            Assert.Equal("trinta e dois", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(40);
            Assert.Equal("quarenta", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(43);
            Assert.Equal("quarenta e tr�s", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(50);
            Assert.Equal("cinquenta", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(54);
            Assert.Equal("cinquenta e quatro", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(60);
            Assert.Equal("sessenta", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(65);
            Assert.Equal("sessenta e cinco", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(70);
            Assert.Equal("setenta", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(76);
            Assert.Equal("setenta e seis", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(80);
            Assert.Equal("oitenta", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(87);
            Assert.Equal("oitenta e sete", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(90);
            Assert.Equal("noventa", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(98);
            Assert.Equal("noventa e oito", extensao);
        }
    }
}
