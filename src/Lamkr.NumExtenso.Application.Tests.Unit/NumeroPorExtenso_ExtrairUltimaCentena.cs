using Lamkr.NumExtenso.Languages;
using Xunit;

namespace Lamkr.NumExtenso.Application.Tests.Unit
{
    public partial class NumeroPorExtensoTests
    {
        [Fact]
        public void NumeroPorExtenso_ExtrairUltimaCentena()
        {
			int centena;

            _numeroPorExtenso.ExtrairUltimaCentena(0, out centena);
			Assert.Equal(0, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(1, out centena);
            Assert.Equal(1, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(19, out centena);
            Assert.Equal(19, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(99, out centena);
            Assert.Equal(99, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(100, out centena);
            Assert.Equal(100, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(999, out centena);
            Assert.Equal(999, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(1000, out centena);
            Assert.Equal(0, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(1001, out centena);
            Assert.Equal(1, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(1019, out centena);
            Assert.Equal(19, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(1099, out centena);
            Assert.Equal(99, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(1100, out centena);
            Assert.Equal(100, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(1999, out centena);
            Assert.Equal(999, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(30000, out centena);
            Assert.Equal(0, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(30001, out centena);
            Assert.Equal(1, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(30019, out centena);
            Assert.Equal(19, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(30099, out centena);
            Assert.Equal(99, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(30100, out centena);
            Assert.Equal(100, centena);

            _numeroPorExtenso.ExtrairUltimaCentena(30999, out centena);
            Assert.Equal(999, centena);
        }
    }
}
