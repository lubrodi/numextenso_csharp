using System;
using Xunit;

namespace Lamkr.NumExtenso.Application.Tests.Unit
{
    public partial class NumeroPorExtensoTests
    {
        private readonly NumeroPorExtenso _numeroPorExtenso;

        public NumeroPorExtensoTests() {
			_numeroPorExtenso = new NumeroPorExtenso();
        }

		[Fact]
		public void NumeroPorExtenso_Inicializacao_Test() {
			Assert.NotEmpty(_numeroPorExtenso.Cardinais);
		}
    }
}
