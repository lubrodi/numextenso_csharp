using Lamkr.NumExtenso.Languages;
using Xunit;

namespace Lamkr.NumExtenso.Application.Tests.Unit
{
    public partial class NumeroPorExtensoTests
    {
        [Fact]
        public void NumeroPorExtenso_DescreverNumero_testar_negativos()
        {
			string extensao;

			extensao = _numeroPorExtenso.DescreverNumero(-0);
			Assert.Equal(Resources.Zero, extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-1);
            Assert.Equal("menos um", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-10);
            Assert.Equal("menos dez", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-15);
            Assert.Equal("menos quinze", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-21);
            Assert.Equal("menos vinte e um", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-67);
            Assert.Equal("menos sessenta e sete", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-100);
            Assert.Equal("menos cem", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-101);
            Assert.Equal("menos cento e um", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-110);
            Assert.Equal("menos cento e dez", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-111);
            Assert.Equal("menos cento e onze", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-150);
            Assert.Equal("menos cento e cinquenta", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-153);
            Assert.Equal("menos cento e cinquenta e tr�s", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-199);
            Assert.Equal("menos cento e noventa e nove", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-200);
            Assert.Equal("menos duzentos", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-234);
            Assert.Equal("menos duzentos e trinta e quatro", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-999);
            Assert.Equal("menos novecentos e noventa e nove", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-1000);
            //!Assert.Equal("menos um mil", extensao);
            Assert.Equal("menos mil", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-1001);
            //!Assert.Equal("menos um mil e um", extensao);
            Assert.Equal("menos mil e um", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-1011);
            //!Assert.Equal("menos um mil e onze", extensao);
            Assert.Equal("menos mil e onze", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-1042);
            //!Assert.Equal("menos um mil e quarenta e dois", extensao);
            Assert.Equal("menos mil e quarenta e dois", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-1054);
            //!Assert.Equal("menos um mil e cinquenta e quatro", extensao);
            Assert.Equal("menos mil e cinquenta e quatro", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-1092);
            //!Assert.Equal("menos um mil e noventa e dois", extensao);
            Assert.Equal("menos mil e noventa e dois", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-10000);
            Assert.Equal("menos dez mil", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-10002);
            Assert.Equal("menos dez mil e dois", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-10013);
            Assert.Equal("menos dez mil e treze", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-10077);
            Assert.Equal("menos dez mil e setenta e sete", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-10100);
            Assert.Equal("menos dez mil e cem", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-10101);
            Assert.Equal("menos dez mil e cento e um", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-10118);
            Assert.Equal("menos dez mil e cento e dezoito", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-10145);
            Assert.Equal("menos dez mil e cento e quarenta e cinco", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-10999);
            Assert.Equal("menos dez mil e novecentos e noventa e nove", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-90000);
            Assert.Equal("menos noventa mil", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-99000);
            Assert.Equal("menos noventa e nove mil", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-94587);
            Assert.Equal("menos noventa e quatro mil e quinhentos e oitenta e sete", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-99900);
            Assert.Equal("menos noventa e nove mil e novecentos", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-99990);
            Assert.Equal("menos noventa e nove mil e novecentos e noventa", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(-99999);
            Assert.Equal("menos noventa e nove mil e novecentos e noventa e nove", extensao);
        }
    }
}
