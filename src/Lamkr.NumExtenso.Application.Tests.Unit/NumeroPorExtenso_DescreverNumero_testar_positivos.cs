using Lamkr.NumExtenso.Languages;
using Xunit;

namespace Lamkr.NumExtenso.Application.Tests.Unit
{
    public partial class NumeroPorExtensoTests
    {
        [Fact]
        public void NumeroPorExtenso_DescreverNumero_testar_positivos()
        {
			string extensao;

			extensao = _numeroPorExtenso.DescreverNumero(0);
			Assert.Equal(Resources.Zero, extensao);

            extensao = _numeroPorExtenso.DescreverNumero(1);
            Assert.Equal("um", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(10);
            Assert.Equal("dez", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(15);
            Assert.Equal("quinze", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(21);
            Assert.Equal("vinte e um", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(67);
            Assert.Equal("sessenta e sete", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(100);
            Assert.Equal(Resources.Cem, extensao);

            extensao = _numeroPorExtenso.DescreverNumero(101);
            Assert.Equal("cento e um", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(110);
            Assert.Equal("cento e dez", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(111);
            Assert.Equal("cento e onze", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(150);
            Assert.Equal("cento e cinquenta", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(153);
            Assert.Equal("cento e cinquenta e tr�s", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(199);
            Assert.Equal("cento e noventa e nove", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(200);
            Assert.Equal("duzentos", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(234);
            Assert.Equal("duzentos e trinta e quatro", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(999);
            Assert.Equal("novecentos e noventa e nove", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(1000);
            //!Assert.Equal("um mil", extensao);
            Assert.Equal("mil", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(1001);
            //!Assert.Equal("um mil e um", extensao);
            Assert.Equal("mil e um", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(1011);
            //!Assert.Equal("um mil e onze", extensao);
            Assert.Equal("mil e onze", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(1054);
            //!Assert.Equal("um mil e cinquenta e quatro", extensao);
            Assert.Equal("mil e cinquenta e quatro", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(1092);
            //!Assert.Equal("um mil e noventa e dois", extensao);
            Assert.Equal("mil e noventa e dois", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(10000);
            Assert.Equal("dez mil", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(10002);
            Assert.Equal("dez mil e dois", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(10013);
            Assert.Equal("dez mil e treze", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(10077);
            Assert.Equal("dez mil e setenta e sete", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(10100);
            Assert.Equal("dez mil e cem", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(10101);
            Assert.Equal("dez mil e cento e um", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(10118);
            Assert.Equal("dez mil e cento e dezoito", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(10145);
            Assert.Equal("dez mil e cento e quarenta e cinco", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(10999);
            Assert.Equal("dez mil e novecentos e noventa e nove", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(90000);
            Assert.Equal("noventa mil", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(99000);
            Assert.Equal("noventa e nove mil", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(99900);
            Assert.Equal("noventa e nove mil e novecentos", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(99990);
            Assert.Equal("noventa e nove mil e novecentos e noventa", extensao);

            extensao = _numeroPorExtenso.DescreverNumero(99999);
            Assert.Equal("noventa e nove mil e novecentos e noventa e nove", extensao);
        }
    }
}
