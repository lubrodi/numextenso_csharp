using System;
using System.Collections.Generic;
using Xunit;

namespace Lamkr.NumExtenso.Application.Tests.Unit
{
    public partial class NumeroPorExtensoTests
    {
        [Fact]
        public void NumeroPorExtenso_DescreverCentena_testar_unidades()
        {
            string extensao = _numeroPorExtenso.DescreverCentena(0);
            Assert.Empty(extensao);

            extensao = _numeroPorExtenso.DescreverCentena(1);
            Assert.Equal("um", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(2);
            Assert.Equal("dois", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(3);
            Assert.Equal("tr�s", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(4);
            Assert.Equal("quatro", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(5);
            Assert.Equal("cinco", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(6);
            Assert.Equal("seis", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(7);
            Assert.Equal("sete", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(8);
            Assert.Equal("oito", extensao);

            extensao = _numeroPorExtenso.DescreverCentena(9);
            Assert.Equal("nove", extensao);

        }
    }
}
