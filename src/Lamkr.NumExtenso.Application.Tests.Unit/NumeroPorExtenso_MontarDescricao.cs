using Lamkr.NumExtenso.Languages;
using Xunit;

namespace Lamkr.NumExtenso.Application.Tests.Unit
{
    public partial class NumeroPorExtensoTests
    {
        [Fact]
        public void NumeroPorExtenso_MontarDescricao()
        {
			string descricao;

            descricao = _numeroPorExtenso.MontarDescricao(0);
			Assert.Empty(descricao);

            descricao = _numeroPorExtenso.MontarDescricao(1);
            Assert.Equal("um", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(10);
            Assert.Equal("dez", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(15);
            Assert.Equal("quinze", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(21);
            Assert.Equal("vinte e um", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(67);
            Assert.Equal("sessenta e sete", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(100);
            Assert.Equal(Resources.Cem, descricao);

            descricao = _numeroPorExtenso.MontarDescricao(101);
            Assert.Equal("cento e um", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(110);
            Assert.Equal("cento e dez", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(111);
            Assert.Equal("cento e onze", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(150);
            Assert.Equal("cento e cinquenta", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(153);
            Assert.Equal("cento e cinquenta e tr�s", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(199);
            Assert.Equal("cento e noventa e nove", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(200);
            Assert.Equal("duzentos", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(234);
            Assert.Equal("duzentos e trinta e quatro", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(999);
            Assert.Equal("novecentos e noventa e nove", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(1000);
            //!Assert.Equal("um mil", descricao);
            Assert.Equal(" mil", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(1001);
            //!Assert.Equal("um mil e um", descricao);
            Assert.Equal(" mil e um", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(1011);
            //!Assert.Equal("um mil e onze", descricao);
            Assert.Equal(" mil e onze", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(1054);
            //!Assert.Equal("um mil e cinquenta e quatro", descricao);
            Assert.Equal(" mil e cinquenta e quatro", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(1092);
            //!Assert.Equal("um mil e noventa e dois", descricao);
            Assert.Equal(" mil e noventa e dois", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(10000);
            Assert.Equal("dez mil", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(10002);
            Assert.Equal("dez mil e dois", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(10013);
            Assert.Equal("dez mil e treze", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(10077);
            Assert.Equal("dez mil e setenta e sete", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(10100);
            Assert.Equal("dez mil e cem", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(10101);
            Assert.Equal("dez mil e cento e um", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(10118);
            Assert.Equal("dez mil e cento e dezoito", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(10145);
            Assert.Equal("dez mil e cento e quarenta e cinco", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(10999);
            Assert.Equal("dez mil e novecentos e noventa e nove", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(90000);
            Assert.Equal("noventa mil", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(99000);
            Assert.Equal("noventa e nove mil", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(99900);
            Assert.Equal("noventa e nove mil e novecentos", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(99990);
            Assert.Equal("noventa e nove mil e novecentos e noventa", descricao);

            descricao = _numeroPorExtenso.MontarDescricao(99999);
            Assert.Equal("noventa e nove mil e novecentos e noventa e nove", descricao);
        }
    }
}
