using Lamkr.NumExtenso.Languages;
using Xunit;

namespace Lamkr.NumExtenso.Application.Tests.Unit
{
    public partial class NumeroPorExtensoTests
    {
        [Fact]
        public void NumeroPorExtenso_ClasseCardinal()
        {
			string descricao;

            descricao = _numeroPorExtenso.ClasseCardinal(NumeroPorExtenso.PrimeiraClasse);
			Assert.Empty(descricao);

            descricao = _numeroPorExtenso.ClasseCardinal(NumeroPorExtenso.SegundaClasse);
            Assert.Equal(Resources.Mil, descricao);

            descricao = _numeroPorExtenso.ClasseCardinal(NumeroPorExtenso.SegundaClasse+1);
            Assert.Equal(Resources.Mil, descricao);
        }
    }
}
