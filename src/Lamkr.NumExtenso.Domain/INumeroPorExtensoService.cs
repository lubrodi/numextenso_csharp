﻿using System;

namespace Lamkr.NumExtenso.Domain
{
    public interface INumeroPorExtensoService
    {
        string DescreverNumero(long numero);
    }
}
