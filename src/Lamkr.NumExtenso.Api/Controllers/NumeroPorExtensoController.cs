﻿using Lamkr.NumExtenso.Api.Results;
using Lamkr.NumExtenso.Domain;
using Microsoft.AspNetCore.Mvc;

namespace Lamkr.NumExtenso.Api.Controllers
{
    [ApiController]
    public class NumeroPorExtensoController : ControllerBase
    {
        private readonly INumeroPorExtensoService _numeroPorExtensoService;

        public NumeroPorExtensoController(INumeroPorExtensoService service) {
            _numeroPorExtensoService = service;
        }

		[HttpGet("{numero}")]
		[Produces("application/json")]
		public ActionResult DescreverNumero(long numero)
		{
			return Ok(new NumeroPorExtensoResult()
			{
				extenso = _numeroPorExtensoService.DescreverNumero(numero)
			});
		}
	}
}
